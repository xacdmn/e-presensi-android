# Teacher Attendance System - Aplikasi Presensi Berbasis QR Code menggunakan Flutter dengan Anti Fake GPS

![Teacher Attendance System Logo](readMe-logo.png)

Selamat datang di repositori Teacher Attendance System! Aplikasi ini adalah sebuah sistem presensi berbasis QR Code yang dirancang khusus untuk para guru. Dengan aplikasi ini, para guru dapat dengan mudah dan efisien melakukan presensi masuk dan pulang menggunakan QR Code. Aplikasi ini juga dilengkapi dengan fitur Anti Fake GPS untuk mencegah penggunaan alat bantu palsu dalam menentukan lokasi presensi.

## Fitur Aplikasi

Aplikasi Teacher Attendance System memiliki beberapa fitur utama yang meliputi:

1. **Halaman Login**
   Halaman ini adalah halaman awal di mana para guru akan memasukkan email dan password untuk masuk ke dalam sistem presensi. Jika email atau password yang dimasukkan salah, akan muncul pesan peringatan berwarna merah yang menandakan bahwa email atau kata sandi yang dimasukkan tidak valid. Proses login juga melibatkan verifikasi device ID untuk mencegah akses tidak sah.

2. **Halaman Beranda**
   Setelah berhasil login, para guru akan diarahkan ke halaman beranda. Halaman ini menampilkan informasi seperti ucapan selamat pagi/siang/sore/malam, tanggal hari ini, dan nama guru yang sedang login. Para guru juga dapat melakukan presensi masuk dan pulang dari halaman ini.

3. **Halaman Izin**
   Pada halaman ini, para guru dapat mengajukan izin dengan memilih jenis izin (sakit/izin), tanggal mulai, tanggal selesai, dan mengunggah surat izin. Selain itu, para guru juga dapat melihat status permohonan izin yang telah diajukan dalam bentuk kartu dengan warna yang berbeda sesuai dengan statusnya.

4. **Halaman Histori**
   Halaman ini menampilkan riwayat presensi para guru berdasarkan bulan yang dipilih. Setiap data riwayat presensi ditampilkan dalam bentuk kartu dengan warna yang berbeda sesuai dengan status presensinya.

5. **Halaman Profil**
   Halaman ini menyajikan informasi penting tentang para guru seperti NUPTK (Nomor Unik Pendidik dan Tenaga Kependidikan), nama lengkap, status guru (PNS, PPPK, atau Guru Honorer), jam kerja, dan lokasi kerja (nama sekolah atau institusi). Para guru juga dapat melihat foto profil mereka dan memiliki opsi untuk keluar dari akun.

6. **Halaman Presensi**
   Halaman ini digunakan untuk melakukan presensi menggunakan QR Code. Para guru dapat memindai QR Code untuk melakukan presensi masuk dan pulang. Sistem akan melakukan validasi terhadap QR Code dan lokasi presensi untuk memastikan integritas data presensi. Fitur Anti Fake GPS akan memberikan peringatan jika ditemukan penggunaan fake GPS.

## Cara Penggunaan

1. Pastikan Anda telah menginstal Flutter SDK di komputer Anda.
2. Clone repositori ini ke direktori lokal Anda.
3. Buka proyek menggunakan editor Flutter, seperti Android Studio atau Visual Studio Code.
4. Jalankan perintah `flutter pub get` di terminal untuk menginstal semua dependensi.
5. Hubungkan perangkat Anda atau jalankan emulator untuk menguji aplikasi.
6. Jalankan aplikasi dengan perintah `flutter run`.

## Admin Website

- **Website**: [Admin TAS](https://presensi.booksinternationals.online)
- **Gitlab**: [Repository Admin TAS](https://gitlab.com/xacdmn/e-presensi-admin)
- Akses admin atau pengguna hubungi kontak dibawah


## Kontribusi

Kami sangat menghargai kontribusi dari para pengembang dan pengguna. Jika Anda ingin berkontribusi, Anda dapat membantu dengan:

- Melaporkan masalah dan bugs yang ditemukan.
- Memberikan saran atau permintaan fitur baru.
- Mengirimkan pull request untuk memperbaiki atau meningkatkan kode aplikasi.

Pastikan untuk selalu mengikuti pedoman kontribusi yang ada dalam repositori ini.


## Kontak

Untuk pertanyaan lebih lanjut atau informasi lebih lanjut tentang aplikasi Teacher Attendance System, Anda dapat menghubungi kami melalui:

- **Email**: [Ellda Artha Airlangga](mailto:elldaartha.ea@gmail.com)
- **Instagram**: [Ellda Artha Airlangga](https://www.instagram.com/ka.el_)
- **Facebook**: [Ellda Artha Airlangga](https://www.facebook.com/Kobee.el28)
- **LinkedIn**: [Ellda Artha Airlangga](https://www.linkedin.com/in/xacdmn)

Terima kasih telah menggunakan Teacher Attendance System! Semoga aplikasi ini membantu meningkatkan efisiensi dan akurasi presensi para guru di sekolah Anda.


## Tampilan Aplikasi
![Teacher Attendance System Logo](ui-aplikasi.png)
