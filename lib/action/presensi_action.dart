import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:presensi/screen/histori_screen.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PresensiScreen extends StatefulWidget {
  const PresensiScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _PresensiScreenState createState() => _PresensiScreenState();
}

class _PresensiScreenState extends State<PresensiScreen> {
  QRViewController? _qrController;
  SharedPreferences? _preferences;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  String dateNow = '';
  String latitude = '0.000000';
  String longitude = '0.000000';
  String timeStamp = '';
  String isPresensi = '';
  String code = '';

  Map<String, dynamic> userData = {};

  final bool canMockLocation = true;
  bool isLoading = true;
  bool isCamera = true;
  bool isProcessing = false;

  StreamSubscription<Position>? _positionStreamSubscription;

  @override
  void initState() {
    checkInternetConnectivity();
    _checkPermission();
    _startLocationUpdates();
    super.initState();
  }

  @override
  void dispose() {
    _stopLocationUpdates();
    _qrController?.dispose();
    super.dispose();
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _qrController?.pauseCamera();
    }
    _qrController?.resumeCamera();
  }

  Future<void> checkInternetConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    Connectivity().onConnectivityChanged.listen((_handleConnectivityResult));
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      setDateNow();
      _getEmailFromSharedPrefs();
      _getCurrentTime();
      setLoading(false);
    }
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  Future<void> _getEmailFromSharedPrefs() async {
    _preferences = await SharedPreferences.getInstance();
    var email = _preferences?.getString('email') ?? '';
    isPresensi = _preferences?.getString('presensi') ?? '';
    await fetchUserProfile(email);
  }

  Future<void> fetchUserProfile(String email) async {
    setLoading(true);
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-profil.php?email=$email');
      var response = await http.get(url);

      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            userData = jsonResponse;
            isLoading = false;
          });
        }
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(true);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet anda tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan: ${e.toString()}');
      }
      setLoading(false);
    }
  }

  void _showErrorToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void _showSuccessToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green,
        textColor: Colors.white,
      );
    }
  }

  Future<void> _startLocationUpdates() async {
    final hasPermission = await Permission.location.request();
    if (hasPermission.isDenied) {
      await Geolocator.requestPermission();
    } else {
      _positionStreamSubscription = Geolocator.getPositionStream(
              locationSettings: const LocationSettings(
                  accuracy: LocationAccuracy.best, distanceFilter: 5))
          .listen((lastPosition) {
        setState(() {
          latitude = lastPosition.latitude.toString();
          longitude = lastPosition.longitude.toString();
          isLoading = false;
        });
      });
    }
  }

  void _stopLocationUpdates() {
    // Stop listening to location updates
    _positionStreamSubscription?.cancel();
  }

  Future<void> _checkPermission() async {
    final cameraStatus = await Permission.camera.status;
    await Geolocator.requestPermission();
    _positionStreamSubscription = Geolocator.getPositionStream(
            locationSettings:
                const LocationSettings(accuracy: LocationAccuracy.best))
        .listen((updatedPosition) async {
      if (updatedPosition.isMocked) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const HomeScreen()),
        );
        _stopLocationUpdates();
        _showErrorToast('Perangkat Anda Terdeteksi Menggunakan Fake GPS');
      } else {
        if (!cameraStatus.isGranted) {
          await Permission.camera.request();
          if (mounted) {
            setState(() {
              isCamera = true;
            });
          }
        }
      }
    });
    if (cameraStatus.isGranted) {
      if (mounted) {
        setState(() {
          isCamera = false;
        });
      }
    }
  }

  void setDateNow() {
    final now = DateTime.now();
    final formatter = DateFormat('yyyy-MM-dd');
    setState(() {
      dateNow = formatter.format(now);
    });
  }

  void _getCurrentTime() {
    final now = DateTime.now();
    final formatter = DateFormat('HH:mm:ss');
    final formattedTime = formatter.format(now);
    setState(() {
      timeStamp = formattedTime;
    });
  }

  void _onQRViewCreated(QRViewController controller) {
    if (mounted) {
      setState(() {
        _qrController = controller;
      });
    }
    _qrController?.scannedDataStream.listen((scanData) {
      if (scanData.code?.isNotEmpty == true) {
        _qrController?.pauseCamera();
        if (mounted) {
          setState(() {
            code = scanData.code.toString();
          });
        }
        if (code.isNotEmpty) {
          _stopLocationUpdates();
          _processQRCode(code);
        } else {
          _showErrorToast('QR code tidak terbaca');
        }
      } else {
        _showErrorToast('QR code tidak terbaca');
      }
    });
  }

  Future<void> _processQRCode(String qrCode) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (mounted &&
        connectivityResult == ConnectivityResult.none &&
        isProcessing) {
      _showErrorToast('Tidak ada koneksi internet');
      if (mounted) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const HomeScreen()),
        );
      }
    } else {
      if (mounted) {
        setState(() {
          isProcessing = true;
        });
      }
      try {
        final url = Uri.parse(
            'https://presensi.booksinternationals.online/api/api-presensi.php?employees_id=${userData['id']}&latitude=$latitude&longitude=$longitude&date=$dateNow&time=$timeStamp&presensi=$isPresensi');

        final response = await http.post(url, body: {'qr_code': qrCode});

        if (response.statusCode == 200) {
          Future.delayed(const Duration(seconds: 2), () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const HistoriScreen()),
            );
            _showSuccessToast(
                'Anda telah berhasil melakukan presensi hari ini');
          });
          if (mounted) {
            setState(() {
              isProcessing = false;
            });
          }
        } else {
          final jsonResponse = jsonDecode(response.body);
          final message = jsonResponse['message'];
          _showErrorToast(message);
          Future.delayed(const Duration(seconds: 2), () {
            _qrController?.resumeCamera();
          });
          if (mounted) {
            setState(() {
              isProcessing = false;
            });
          }
        }
      } catch (e) {
        if (e is Exception) {
          _showErrorToast('Koneksi internet anda tidak stabil');
        } else {
          _showErrorToast('Terjadi kesalahan: ${e.toString()}');
        }
        setLoading(isLoading);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final isSmallScreen = screenWidth <= 600;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: isLoading
          ? null
          : AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.blue,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 8.0 * MediaQuery.of(context).textScaleFactor),
                  const Text(
                    'Scan QR Code',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              elevation: 0, // Set elevation ke 0 untuk menghapus shadow
            ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Column(
                children: [
                  Expanded(
                    flex: 2,
                    child: isCamera
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : QRView(
                            key: qrKey,
                            onQRViewCreated: _onQRViewCreated,
                            overlay: QrScannerOverlayShape(
                              borderRadius: 10,
                              borderColor: Colors.red,
                              borderLength: 20,
                              borderWidth: 10,
                              cutOutSize: 200,
                            ),
                          ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Arahkan kamera ke QR Code',
                              style: TextStyle(
                                fontSize: 20.0 *
                                    MediaQuery.of(context).textScaleFactor,
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                top: 30.0 *
                                    MediaQuery.of(context).textScaleFactor)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "${latitude.substring(0, 8)}, ${longitude.substring(0, 8)}",
                              style: TextStyle(
                                  fontSize: 20 *
                                      MediaQuery.of(context).textScaleFactor,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomeScreen()),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      padding: EdgeInsets.symmetric(
                        vertical: isSmallScreen ? 2.0 : 5.0,
                        horizontal: isSmallScreen ? 11.0 : 25.0,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                    child: Text(
                      'Kembali ke beranda',
                      style: TextStyle(
                        fontSize: isSmallScreen ? 10.0 : 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
