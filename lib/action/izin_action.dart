import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/date_symbol_data_local.dart';
import 'package:file_picker/file_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:presensi/screen/histori_screen.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:presensi/screen/izin_screen.dart';
import 'package:presensi/screen/profil_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/navbar.dart';

class FormIzinScreen extends StatefulWidget {
  const FormIzinScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _FormIzinScreenState createState() => _FormIzinScreenState();
}

class _FormIzinScreenState extends State<FormIzinScreen> {
  SharedPreferences? _preferences;
  File? _selectedFile;

  int selectedIndex = 1;

  String currentName = '';
  String currentUser = '';
  String dateNow = '';
  String firstName = '';
  String greeting = '';
  String _selectedStatus = 'Izin';

  DateTime _selectedStartDate = DateTime.now();
  DateTime _selectedEndDate = DateTime.now();

  final List<String> _statusOptions = ['Sakit', 'Izin'];
  Stream<String> currentTimeInSeconds = Stream<String>.periodic(
    const Duration(seconds: 1),
    (count) {
      final now = DateTime.now();
      return '${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}:${now.second.toString().padLeft(2, '0')}';
    },
  );

  bool isLoading = true;
  bool isProcessing = false;

  @override
  void initState() {
    super.initState();
    _checkInternetConnectivity();
  }

  Future<void> _checkInternetConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    Connectivity().onConnectivityChanged.listen((_handleConnectivityResult));
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      _getCurrentUserFromSharedPrefs();
      initializeDateFormatting('id_ID', null);
      setGreeting();
      setDateNow();
      setLoading(false);
    }
  }

  void setGreeting() {
    final now = DateTime.now();
    if (now.hour >= 0 && now.hour < 12) {
      greeting = 'Selamat Pagi';
    } else if (now.hour >= 12 && now.hour < 15) {
      greeting = 'Selamat Siang';
    } else if (now.hour >= 15 && now.hour < 18) {
      greeting = 'Selamat Sore';
    } else {
      greeting = 'Selamat Malam';
    }
  }

  void setDateNow() {
    dateNow = DateFormat('d MMM y').format(DateTime.now());
  }

  void setFirstName(String currentName) {
    firstName = currentName.split(' ')[0];
  }

  Future<void> _selectDateRange(BuildContext context) async {
    final currentDate = DateTime.now();
    final initialDateRange = DateTimeRange(
      start: _selectedStartDate,
      end: _selectedEndDate,
    );

    final pickedDateRange = await showDateRangePicker(
      context: context,
      firstDate: currentDate,
      lastDate: DateTime(currentDate.year, 12, 31),
      initialDateRange: initialDateRange.start.isBefore(currentDate)
          ? DateTimeRange(
              start: currentDate,
              end: currentDate,
            )
          : initialDateRange,
      initialEntryMode: DatePickerEntryMode.calendarOnly,
      locale: const Locale('id', 'ID'),
    );

    if (pickedDateRange != null && pickedDateRange != initialDateRange) {
      setState(() {
        _selectedStartDate = pickedDateRange.start;
        _selectedEndDate = pickedDateRange.end;
      });
    }
  }

  Future<void> _getCurrentUserFromSharedPrefs() async {
    _preferences = await SharedPreferences.getInstance();
    var currentUser = _preferences?.getString('id') ?? '';
    var currentName = _preferences?.getString('employees_name') ?? '';
    setState(() {
      this.currentName = currentName;
      this.currentUser = currentUser;
      setFirstName(currentName);
    });
  }

  Future<void> _selectFile(BuildContext context) async {
    try {
      final PermissionStatus status = await Permission.storage.request();
      if (status.isGranted) {
        FilePickerResult? result = await FilePicker.platform.pickFiles(
          type: FileType.custom,
          allowedExtensions: ['doc', 'pdf', 'jpeg', 'jpg', 'docx', 'docm'],
        );
        if (result != null) {
          File selectedFile = File(result.files.single.path!);
          if (selectedFile.existsSync()) {
            // Lanjutkan dengan penggunaan file
          } else {
            _showErrorToast('File tidak sumber ditemukan');
          }
          setState(() {
            _selectedFile = File(result.files.single.path!);
          });
        }
      } else {
        _showErrorToast('Izin Akses File Diperlukan');
        Future.delayed(const Duration(seconds: 2), () {
          _openAppSettings();
        });
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet anda tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan: ${e.toString()}');
      }
    }
  }

  Future<void> _addIzin(File selectedFile) async {
    if (!_selectedStatus.isNotEmpty) {
      _showErrorToast('Silahkan pilih file terlebih dahulu!');
      return;
    } else {
      if (mounted) {
        setState(() {
          isProcessing = true;
        });
      }
      try {
        String startDateFormatted =
            '${_selectedStartDate.year}-${_selectedStartDate.month.toString().padLeft(2, '0')}-${_selectedStartDate.day.toString().padLeft(2, '0')}';
        String endDateFormatted =
            '${_selectedEndDate.year}-${_selectedEndDate.month.toString().padLeft(2, '0')}-${_selectedEndDate.day.toString().padLeft(2, '0')}';

        String fileName = path.basename(selectedFile.path);
        FormData formData = FormData.fromMap({
          "employees_id": currentUser,
          "permission_name": currentName,
          "permission_date": startDateFormatted,
          "permission_date_finish": endDateFormatted,
          "type": _selectedStatus,
          "file": await MultipartFile.fromFile(selectedFile.path,
              filename: fileName),
        });

        Response response = await Dio().post(
            "https://presensi.booksinternationals.online/api/api-post-izin.php",
            data: formData);
        if (response.statusCode == 200) {
          var jsonResponse = jsonDecode(response.data);
          bool status = jsonResponse['status'];
          String message = jsonResponse['message'];

          if (status) {
            _showSuccessToast('Pengajuan telah izin dikirim!');
            // ignore: use_build_context_synchronously
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const IzinScreen()),
            );
            if (mounted) {
              setState(() {
                isProcessing = false;
              });
            }
          } else {
            _showErrorToast(
                'Terjadi kesalahan saat mengirim pengajuan: $message');
            // ignore: use_build_context_synchronously
            Navigator.of(context).pop();
            if (mounted) {
              setState(() {
                isProcessing = false;
              });
            }
          }
        } else {
          _showErrorToast(
              'Terjadi kesalahan saat mengirim pengajuan silahkan coba lagi!');
          // ignore: use_build_context_synchronously
          Navigator.of(context).pop();
          if (mounted) {
            setState(() {
              isProcessing = false;
            });
          }
        }
      } catch (e) {
        if (e is Exception) {
          _showErrorToast('Koneksi internet anda tidak stabil');
        } else {
          _showErrorToast('Terjadi kesalahan: ${e.toString()}');
        }
        setLoading(false);
      } finally {
        if (mounted) {
          setState(() {
            isProcessing = false;
          });
        }
      }
    }
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  void _showSuccessToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green,
        textColor: Colors.white,
      );
    }
  }

  void _showErrorToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.red,
      textColor: Colors.white,
    );
    Navigator.of(context).pop();
  }

  void _openAppSettings() {
    AppSettings.openAppSettings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 227, 227, 227),
      appBar: isLoading
          ? null
          : AppBar(
              backgroundColor: Colors.blue,
              automaticallyImplyLeading: false,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 8.0 * MediaQuery.of(context).textScaleFactor),
                  const Text(
                    'Formulir Pengajuan Izin',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              elevation: 0, // Set elevation ke 0 untuk menghapus shadow
            ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 11.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Card(
                    margin: const EdgeInsets.fromLTRB(16.0, 40.0, 16.0, 16.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                greeting,
                                style: const TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 8.0),
                              Text(
                                dateNow,
                                style: const TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 8.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Halo $firstName!',
                                style: const TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 8.0),
                              StreamBuilder<String>(
                                stream: currentTimeInSeconds,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    String currentTime = snapshot.data!;
                                    return Text(
                                      currentTime,
                                      style: const TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    );
                                  } else {
                                    return const Text('Memuat...');
                                  }
                                },
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 12.0,
                          ),
                          const Divider(
                            color: Colors.grey,
                            height: 1,
                            thickness: 1,
                            indent: 1,
                            endIndent: 1,
                          ),
                          const SizedBox(height: 16.0),
                          const Text('Keterangan'),
                          const SizedBox(height: 8.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color:
                                            Colors.grey, // Set the border color
                                        width: 1.0, // Set the border width
                                      ),
                                      borderRadius: BorderRadius.circular(
                                          4.0), // Set the border radius
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 14.0, right: 23.0),
                                      child: DropdownButton<String>(
                                        value: _selectedStatus,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedStatus = newValue!;
                                          });
                                        },
                                        items: _statusOptions
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: const TextStyle(
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          );
                                        }).toList(),
                                        iconEnabledColor: Colors.transparent,
                                        underline:
                                            Container(), // Remove the default underline
                                      ),
                                    )),
                              )
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          const Text('Mulai Tanggal'),
                          const SizedBox(height: 8.0),
                          Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () => _selectDateRange(context),
                                  child: InputDecorator(
                                      decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8.0,
                                          vertical: 4.0,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5.0, right: 23.0),
                                        child: Text(
                                            DateFormat.yMMMMd('id_ID')
                                                .format(_selectedStartDate),
                                            style: const TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.normal,
                                            )),
                                      )),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          const Text('Sampai Tanggal'),
                          const SizedBox(height: 8.0),
                          Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  // onTap: () => _selectDateRange(context),
                                  child: InputDecorator(
                                      decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8.0,
                                          vertical: 4.0,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 5.0, right: 23.0),
                                        child: Text(
                                            DateFormat.yMMMMd('id_ID')
                                                .format(_selectedEndDate),
                                            style: const TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.normal,
                                            )),
                                      )),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    _selectFile(context);
                                  },
                                  child: InputDecorator(
                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 8.0,
                                        vertical: 4.0,
                                      ),
                                    ),
                                    child: _selectedFile != null
                                        ? Column(
                                            children: [
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              const Text(
                                                'Upload Surat Keterangan Izin/Sakit',
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              const Text(
                                                'jpg, jpeg, doc atau pdf',
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.grey,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 20.0,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors
                                                        .grey, // Set the border color
                                                    width:
                                                        1.0, // Set the border width
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          4.0), // Set the border radius
                                                ),
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Text(
                                                          'File: ${path.basename(_selectedFile!.path)}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 14.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    IconButton(
                                                      icon: const Icon(
                                                        Icons.clear,
                                                        size: 15.0,
                                                        color: Colors.red,
                                                      ),
                                                      onPressed: () {
                                                        setState(() {
                                                          _selectedFile = null;
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                            ],
                                          )
                                        : Column(
                                            children: [
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              const Text(
                                                'Upload Surat Keterangan Izin/Sakit',
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              const Text(
                                                'jpg, jpeg, doc atau pdf',
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.grey,
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 20.0,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors
                                                        .blue, // Set the border color
                                                    width:
                                                        1.5, // Set the border width
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          4.0), // Set the border radius
                                                ),
                                                child: const Padding(
                                                  padding: EdgeInsets.all(8.0),
                                                  child: Text(
                                                    'Pilih File',
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                            ],
                                          ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Center(
                            child: Visibility(
                              visible: isProcessing,
                              replacement: ElevatedButton(
                                onPressed: () {
                                  if (_selectedFile != null) {
                                    _addIzin(_selectedFile!);
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title:
                                              const Text('File Belum Dipilih'),
                                          content: const Text(
                                              'Silakan pilih file terlebih dahulu.'),
                                          actions: [
                                            TextButton(
                                              child: const Text('Tutup'),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                    return;
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.blue,
                                  padding: const EdgeInsets.all(
                                    8.0,
                                  ),
                                ),
                                child: const Text(
                                  'Ajukan Surat Izin',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              child: const CircularProgressIndicator(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ),
      bottomNavigationBar: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: CustomBottomNavigationBar(
          selectedIndex: selectedIndex,
          onIndexChanged: (index) {
            selectedIndex = index;
            switch (index) {
              case 0:
                // Navigate to  home
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
                break;
              case 1:
                // Navigate to Izin
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const IzinScreen()),
                );
                break;
              case 2:
                // Navigate to Histori
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HistoriScreen()),
                );
                break;
              case 3:
                // Stay on Profil
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilScreen()),
                );
                break;
            }
          },
        ),
      ),
    );
  }
}
