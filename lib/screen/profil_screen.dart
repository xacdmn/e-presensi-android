import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:presensi/screen/histori_screen.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:presensi/screen/izin_screen.dart';
import 'package:presensi/screen/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/navbar.dart';

class ProfilScreen extends StatefulWidget {
  const ProfilScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _ProfilScreenState createState() => _ProfilScreenState();
}

class _ProfilScreenState extends State<ProfilScreen> {
  SharedPreferences? _preferences;
  int selectedIndex = 3; // set default value ke Profil
  Map<String, dynamic> userData = {};
  bool isLoading = true;
  final connectivity = Connectivity();

  @override
  void initState() {
    super.initState();
    _checkInternetConnectivity();
  }

  Future<void> _checkInternetConnectivity() async {
    var connectivityResult = await connectivity.checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    connectivity.onConnectivityChanged.listen(_handleConnectivityResult);
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      _getEmailFromSharedPrefs();
      setLoading(false);
    }
  }

  Future<void> _getEmailFromSharedPrefs() async {
    _preferences = await SharedPreferences.getInstance();
    var email = _preferences?.getString('email') ?? '';
    fetchUserProfile(email);
  }

  Future<void> fetchUserProfile(String email) async {
    setLoading(true);
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-profil.php?email=$email'); // Ganti dengan URL API yang sesuai
      var response = await http.get(url);

      if (response.statusCode == 200) {
        _processUserData(response.body);
      } else {
        _handleError('Koneksi internet anda tidak stabil');
      }
    } catch (e) {
      _handleError('Terjadi kesalahan: ${e.toString()}');
    }
  }

  void _processUserData(String responseBody) {
    var jsonResponse = jsonDecode(responseBody);
    setState(() {
      userData = jsonResponse;
      isLoading = false;
    });

    if (userData['time_in'] != null) {
      String timeIn = userData['time_in'];
      List<String> timeParts = timeIn.split(':');
      int hours = int.parse(timeParts[0]);
      int minutes = int.parse(timeParts[1]);
      int seconds = int.parse(timeParts[2]);
      int totalMinutes = (hours * 60) + minutes;
      totalMinutes -= 15; // Mengurangi 10 menit
      if (totalMinutes < 0) {
        totalMinutes += (24 * 60); // Mengubah ke format 24 jam
      }
      hours = totalMinutes ~/ 60;
      minutes = totalMinutes % 60;
      String updatedTimeIn =
          '${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
      userData['time_in'] = updatedTimeIn;
    }
  }

  Future<void> _logout() async {
    _preferences = await SharedPreferences.getInstance();
    await _preferences?.clear();
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
    );
  }

  void _showErrorToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.red,
      textColor: Colors.white,
    );
  }

  void setLoading(bool state) {
    setState(() {
      isLoading = state;
    });
  }

  void _handleError(String errorMessage) {
    _showErrorToast(errorMessage);
    setLoading(false);
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final isSmallScreen = screenHeight <= 600;
    final double cardPadding = 12.0 * MediaQuery.of(context).textScaleFactor;

    // Define responsive padding based on screen size
    final double circleAvatarRadius = isSmallScreen ? 50.0 : 70.0;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Padding(
                padding: EdgeInsets.all(cardPadding),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 0.08 * screenHeight),
                    ),
                    CircleAvatar(
                      radius: circleAvatarRadius,
                      backgroundImage:
                          const AssetImage('assets/images/profil.png'),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 0.02 * screenHeight),
                    ),
                    SizedBox(height: isSmallScreen ? 8.0 : 16.0),
                    buildInfoCard(
                      label: 'NUPTK',
                      value: userData['employees_nip'] ?? '',
                      icon: Icons.badge,
                    ),
                    buildInfoCard(
                      label: 'Nama',
                      value: userData['employees_name'] ?? '',
                      icon: Icons.person,
                    ),
                    buildInfoCard(
                      label: 'Status',
                      value: userData['position_name'] ?? '',
                      icon: Icons.work,
                    ),
                    buildInfoCard(
                      label: 'Jam Kerja',
                      value: '${userData['time_in']} - ${userData['time_out']}',
                      icon: Icons.access_time,
                    ),
                    buildInfoCard(
                      label: 'Lokasi Kerja',
                      value: userData['building_name'] ?? '',
                      icon: Icons.location_on,
                    ),
                    const SizedBox(height: 16.0),
                    ElevatedButton(
                      onPressed: () {
                        _logout();
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        padding: EdgeInsets.symmetric(
                          vertical: isSmallScreen ? 2.0 : 5.0,
                          horizontal: isSmallScreen ? 11.0 : 25.0,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      child: Text(
                        'Keluar',
                        style: TextStyle(
                          fontSize: isSmallScreen ? 10.0 : 12.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
      bottomNavigationBar: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: CustomBottomNavigationBar(
          selectedIndex: selectedIndex,
          onIndexChanged: (index) {
            if (mounted) {
              setState(() {
                selectedIndex = index;
              });
            }
            // Handle navigation to different screens based on the selected menu item index
            switch (index) {
              case 0:
                // Navigate to  home
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
                break;
              case 1:
                // Navigate to Izin
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const IzinScreen()),
                );
                break;
              case 2:
                // Navigate to Histori
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HistoriScreen()),
                );
                break;
              case 3:
                // Stay on Profil
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilScreen()),
                );
                break;
            }
          },
        ),
      ),
    );
  }

  Widget buildInfoCard({
    required String label,
    required String value,
    required IconData icon,
  }) {
    final screenWidth = MediaQuery.of(context).size.width;
    final isSmallScreen = screenWidth <= 600;

    // Define responsive padding and font sizes based on screen size
    final double horizontalPadding = isSmallScreen ? 8.0 : 16.0;
    final double verticalPadding = isSmallScreen ? 8.0 : 16.0;
    final double labelFontSize = isSmallScreen ? 14.0 : 16.0;
    final double valueFontSize = isSmallScreen ? 14.0 : 16.0;

    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: horizontalPadding,
          vertical: verticalPadding,
        ),
        child: Row(
          children: [
            Icon(icon),
            SizedBox(width: horizontalPadding),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    label,
                    style: TextStyle(
                      fontSize: labelFontSize,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: verticalPadding / 2),
                  Text(
                    value,
                    style: TextStyle(
                      fontSize: valueFontSize,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
