import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/date_symbol_data_local.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import 'package:presensi/action/izin_action.dart';
import 'package:presensi/screen/histori_screen.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:presensi/screen/profil_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../util/navbar.dart';

class IzinScreen extends StatefulWidget {
  const IzinScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _IzinScreenState createState() => _IzinScreenState();
}

class _IzinScreenState extends State<IzinScreen> {
  SharedPreferences? _preferences;
  int selectedIndex = 1;
  String currentUser = '';
  String selectedMonth = '';
  String currentYear = '';
  bool isLoading = true;
  List<Map<String, dynamic>> historiIzin = [];

  List<String> months = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ];

  @override
  void initState() {
    super.initState();
    _checkInternetConnectivity();
  }

  Future<void> _checkInternetConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    Connectivity().onConnectivityChanged.listen((_handleConnectivityResult));
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      setCurrentMonth();
      setCurrentYear();
      _getCurrentUserFromSharedPrefs();
      initializeDateFormatting('id_ID', null).then((_) {
        fetchHistoriIzin(selectedMonth, currentYear);
      });
      setLoading(false);
    }
  }

  Future<void> _getCurrentUserFromSharedPrefs() async {
    _preferences = await SharedPreferences.getInstance();
    var currentUser = _preferences?.getString('id') ?? '';
    if (mounted) {
      setState(() {
        this.currentUser = currentUser;
      });
    }
  }

  Future<void> fetchHistoriIzin(String month, String year) async {
    setLoading(true);
    if (month.length > 2) {
      int monthIndex = months.indexOf(month) + 1;
      month = monthIndex.toString();
    }
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-izin.php?employees_id=$currentUser&month=$month&year=$year');
      var response = await http.get(url);

      if (response.statusCode == 200) {
        List<Map<String, dynamic>> allHistoriIzin =
            List<Map<String, dynamic>>.from(json.decode(response.body));
        DateTime today = DateTime.now();
        if (mounted) {
          setState(() {
            historiIzin = allHistoriIzin.where((histori) {
              String dateIzin = histori['date'];
              DateTime date = DateFormat('dd-MM-yyyy').parse(dateIzin);
              return date.isBefore(today) || date.isAtSameMomentAs(today);
            }).toList();
          });
          setLoading(false);
        }
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(true);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet anda tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan: ${e.toString()}');
      }
      setLoading(false);
    }
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  void _showErrorToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void setCurrentMonth() {
    final now = DateTime.now();
    int indexOfMonth = now.month;
    selectedMonth = months.elementAt(indexOfMonth - 1);
  }

  void setCurrentYear() {
    final now = DateTime.now();
    currentYear = now.year.toString();
  }

  String formatDate(String date) {
    DateTime parsedDate = DateFormat('yyyy-MM-dd').parse(date);
    return DateFormat('EEEE, d MMMM y', 'id_ID').format(parsedDate);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: isLoading
          ? null
          : AppBar(
              backgroundColor: Colors.blue,
              automaticallyImplyLeading: false,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 8.0 * MediaQuery.of(context).textScaleFactor),
                  const Text(
                    'Riwayat Izin',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              elevation: 0, // Set elevation ke 0 untuk menghapus shadow
            ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(
                            10.0 * MediaQuery.of(context).textScaleFactor),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: EdgeInsets.all(
                          16.0 * MediaQuery.of(context).textScaleFactor),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Izin Bulan: ', // Added title
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0 *
                                      MediaQuery.of(context).textScaleFactor,
                                ),
                              ),
                              SizedBox(
                                  width: 8.0 *
                                      MediaQuery.of(context).textScaleFactor),
                              DropdownButton<String>(
                                value: selectedMonth,
                                onChanged: (String? newValue) {
                                  // print('Selected Month: $selectedMonth');
                                  if (mounted) {
                                    setState(() {
                                      selectedMonth = newValue!;
                                    });
                                  }
                                  // Konversi nama bulan menjadi nilai numerik
                                  int monthIndex =
                                      months.indexOf(selectedMonth) + 1;
                                  fetchHistoriIzin(
                                    monthIndex.toString(),
                                    currentYear,
                                  );
                                },
                                items: months.map<DropdownMenuItem<String>>(
                                  (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                          fontSize: 16.0 *
                                              MediaQuery.of(context)
                                                  .textScaleFactor,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                              ),
                              SizedBox(
                                  width: 8.0 *
                                      MediaQuery.of(context).textScaleFactor),
                              Text(
                                currentYear,
                                style: TextStyle(
                                  fontSize: 16.0 *
                                      MediaQuery.of(context).textScaleFactor,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                              height: 16.0 *
                                  MediaQuery.of(context).textScaleFactor),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const FormIzinScreen()),
                              );
                            },
                            child: Text(
                              'Tambah Data Izin',
                              style: TextStyle(
                                fontSize: 10.0 *
                                    MediaQuery.of(context).textScaleFactor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: historiIzin.isEmpty
                        ? Center(
                            child: Text(
                              'Tidak ada data permohonan izin.',
                              style: TextStyle(
                                fontSize: 16.0 *
                                    MediaQuery.of(context).textScaleFactor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        : isLoading
                            ? const Center(
                                child: CircularProgressIndicator(),
                              ) // Show a loading indicator while fetching data
                            : ListView.builder(
                                itemCount: historiIzin.length,
                                itemBuilder: (context, index) {
                                  var histori = historiIzin[index];
                                  String currentName =
                                      histori['permission_name'];
                                  String startDate =
                                      histori['start_date'] ?? '';
                                  String finishDate = histori['finish_date'];
                                  String type = histori['type'];
                                  String statusName =
                                      histori['status_name'] ?? '';
                                  String date = histori['date'];

                                  return Padding(
                                    padding: EdgeInsets.all(8.0 *
                                        MediaQuery.of(context).textScaleFactor),
                                    child: Card(
                                      elevation: 2,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            8.0 *
                                                MediaQuery.of(context)
                                                    .textScaleFactor),
                                      ),
                                      color: statusName == 'Menunggu'
                                          ? Colors.yellow[800]
                                          : statusName == 'Diterima'
                                              ? Colors.green
                                              : Colors.red,
                                      child: Padding(
                                        padding: EdgeInsets.all(12.0 *
                                            MediaQuery.of(context)
                                                .textScaleFactor),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                    height: 8.0 *
                                                        MediaQuery.of(context)
                                                            .textScaleFactor),
                                                Text(
                                                  formatDate(date),
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 15.0 *
                                                          MediaQuery.of(context)
                                                              .textScaleFactor,
                                                      color: Colors.white),
                                                ),
                                                SizedBox(
                                                    height: 8.0 *
                                                        MediaQuery.of(context)
                                                            .textScaleFactor),
                                                Text(
                                                  'Nama: $currentName',
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                                SizedBox(
                                                    height: 4.0 *
                                                        MediaQuery.of(context)
                                                            .textScaleFactor),
                                                Text(
                                                  'Mulai: ${formatDate(startDate)}',
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                                SizedBox(
                                                    height: 4.0 *
                                                        MediaQuery.of(context)
                                                            .textScaleFactor),
                                                Text(
                                                  'Selesai: ${formatDate(finishDate)}',
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                                SizedBox(
                                                    height: 4.0 *
                                                        MediaQuery.of(context)
                                                            .textScaleFactor),
                                                Text(
                                                  'Status: $type',
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              statusName,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0 *
                                                    MediaQuery.of(context)
                                                        .textScaleFactor,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                  ),
                ],
              ),
      ),
      bottomNavigationBar: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: CustomBottomNavigationBar(
          selectedIndex: selectedIndex,
          onIndexChanged: (index) {
            selectedIndex = index;
            switch (index) {
              case 0:
                // Navigate to  home
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
                break;
              case 1:
                // Navigate to Izin
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const IzinScreen()),
                );
                break;
              case 2:
                // Navigate to Histori
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HistoriScreen()),
                );
                break;
              case 3:
                // Stay on Profil
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilScreen()),
                );
                break;
            }
          },
        ),
      ),
    );
  }
}
