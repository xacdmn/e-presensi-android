import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:crypto/crypto.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:presensi/screen/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final double getSmallDiameter = Get.width * 2 / 3;
  final double getBigDiameter = Get.size.width * 7 / 8;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  String deviceID = '';
  bool isLoading = false;
  bool isPasswordVisible = false;
  dynamic responseData;

  @override
  void initState() {
    super.initState();
    _getDeviceInfo();
  }

  Future<void> _login() async {
    String email = _emailController.text;
    String password = _passwordController.text;

    if (email.isEmpty || password.isEmpty) {
      _showErrorToast('Email atau password tidak boleh kosong!');
      return;
    }

    if (deviceID.isEmpty) {
      _showErrorToast(
          'Perangkat anda tidak terdeteksi. Mohon jangan menggunakan akses terlarang (rooting).');
      return;
    }

    try {
      var connectivityResult = await Connectivity().checkConnectivity();
      if (connectivityResult != ConnectivityResult.none) {
        final response = await http.post(
            Uri.parse(
                'https://presensi.booksinternationals.online/api/api-login.php'),
            body: {
              'email': email,
              'password': password,
              'deviceID': deviceID,
            });
        if (response.statusCode == 200) {
          final data = json.decode(response.body);
          if (data['success'] == 'true') {
            _showLoginSuccessToast(data['message']);
            responseData = data;
            _saveLoginData();
          } else {
            _showErrorToast(data['message']);
          }
        } else {
          _showErrorToast('Koneksi internet anda tidak stabil');
        }
      } else {
        _showErrorToast('Tidak ada koneksi internet');
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast(e.toString());
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
    }
  }

  Future<void> _getDeviceInfo() async {
    try {
      await Future.delayed(
          const Duration(seconds: 5)); // Contoh tugas delay selama 2 detik

      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String deviceID;
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

      deviceID = androidInfo.id + androidInfo.serialNumber;

      await preferences.setString(
          'deviceID', (md5.convert(utf8.encode(deviceID))).toString());
      setState(() {
        this.deviceID = (md5.convert(utf8.encode(deviceID))).toString();
      });
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
    }
  }

  Future<void> _saveLoginData() async {
    try {
      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      await preferences.setString('email', _emailController.text);
      await preferences.setString('password', _passwordController.text);
      await preferences.setBool('isLoggedIn', true);

      if (responseData != null && responseData['id'] != null) {
        await preferences.setString(
            'id', responseData['id']); // Access the stored data here
      }

      if (responseData != null && responseData['employees_name'] != null) {
        await preferences.setString(
            'employees_name', responseData['employees_name']);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
    }
  }

  void _showLoginSuccessToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green,
        textColor: Colors.white,
      );
      setLoading(true);
      Future.delayed(const Duration(seconds: 3), () {
        if (mounted) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const HomeScreen()),
          );
        }
      });
    }
  }

  void _showErrorToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
    setLoading(false);
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  Widget _buildEmailForm() {
    return TextFormField(
      controller: _emailController,
      decoration: InputDecoration(
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.blueAccent)),
          prefixIcon: Icon(
            Icons.email,
            color: Colors.blueAccent[700],
          ),
          labelText: 'Email',
          labelStyle: TextStyle(color: Colors.blueAccent[200])),
      style: TextStyle(color: Colors.blueAccent[700]),
    );
  }

  Widget _buildPasswordForm() {
    return TextFormField(
      obscureText: !isPasswordVisible,
      controller: _passwordController,
      decoration: InputDecoration(
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.blueAccent)),
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                isPasswordVisible = !isPasswordVisible;
              });
            },
            child: Icon(
              isPasswordVisible ? Icons.visibility_off : Icons.visibility,
              color: Colors.blueAccent[700],
            ),
          ),
          prefixIcon: Icon(
            Icons.lock_outline,
            color: Colors.blueAccent[700],
          ),
          labelText: 'Password',
          labelStyle: TextStyle(color: Colors.blueAccent[200])),
      style: TextStyle(color: Colors.blueAccent[700]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: <Widget>[
          Positioned(
            right: -getSmallDiameter / 3,
            top: -getSmallDiameter / 3,
            child: Container(
              width: getSmallDiameter,
              height: getSmallDiameter,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(colors: [
                  Color.fromARGB(255, 0, 110, 255),
                  Color.fromARGB(255, 93, 150, 249)
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
              ),
            ),
          ),
          Positioned(
            left: -getBigDiameter / 6,
            top: -getBigDiameter / 4,
            child: Container(
              width: getBigDiameter,
              height: getBigDiameter,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(colors: [
                  Color.fromARGB(255, 4, 94, 212),
                  Color.fromARGB(255, 57, 128, 249)
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 40.0),
                  Image.asset(
                    'assets/images/home-logo.png',
                    width: 170,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ListView(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(6),
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(5.0, 350, 5.0, 10),
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 25),
                    child: Card(
                      elevation: 6.0,
                      child: Column(
                        children: <Widget>[
                          _buildEmailForm(),
                          _buildPasswordForm()
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        height: 40.0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            gradient: const LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 0, 166, 255),
                                  Color.fromARGB(255, 0, 80, 141)
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight),
                          ),
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10.0),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(10.0),
                              onTap: isLoading
                                  ? null
                                  : () async {
                                      setLoading(true);
                                      final connectivityResult =
                                          await Connectivity()
                                              .checkConnectivity();
                                      if (connectivityResult !=
                                          ConnectivityResult.none) {
                                        _login();
                                      } else {
                                        _showErrorToast(
                                            'Tidak ada koneksi internet');
                                      }
                                    },
                              child: Center(
                                child: isLoading
                                    ? const SizedBox(
                                        height: 30.0,
                                        width: 30.0,
                                        child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation(
                                              Colors.white),
                                        ),
                                      )
                                    : const Text(
                                        'MASUK',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600),
                                      ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Text(
                      'version 1.7.0',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      'Teacher Attedance System by Ellda Artha Airlangga ',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12.0,
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
