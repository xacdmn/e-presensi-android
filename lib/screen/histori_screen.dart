import 'dart:convert';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/date_symbol_data_local.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:presensi/screen/izin_screen.dart';
import 'package:presensi/screen/profil_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

import '../util/navbar.dart';

class HistoriScreen extends StatefulWidget {
  const HistoriScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _HistoriScreenState createState() => _HistoriScreenState();
}

class _HistoriScreenState extends State<HistoriScreen> {
  SharedPreferences? _preferences;
  int selectedIndex = 2;
  bool isLoading = true;
  List<Map<String, dynamic>> historiPresensi = [];
  List<String> months = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ];

  String currentUser = '';
  String selectedMonth = '';
  String currentYear = '';

  @override
  void initState() {
    super.initState();
    _checkInternetConnectivity();
  }

  Future<void> _checkInternetConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    Connectivity().onConnectivityChanged.listen((_handleConnectivityResult));
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      setCurrentMonth();
      setCurrentYear();
      initializeDateFormatting('id_ID', null).then((_) {
        fetchHistoriPresensi(selectedMonth, currentYear);
      });
      setLoading(false);
    }
  }

  Future<void> _getCurrentUserFromSharedPrefs() async {
    _preferences = await SharedPreferences.getInstance();
    var currentUser = _preferences?.getString('id') ?? '';
    if (mounted) {
      setState(() {
        this.currentUser = currentUser;
      });
    }
  }

  Future<void> fetchHistoriPresensi(String month, String year) async {
    setLoading(true);
    await _getCurrentUserFromSharedPrefs();
    try {
      if (month.length > 2) {
        int monthIndex = months.indexOf(month) + 1;
        month = monthIndex.toString();
      }
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-history.php?employees_id=$currentUser&month=$month&year=$year');
      var response = await http.get(url);

      if (response.statusCode == 200) {
        List<Map<String, dynamic>> allHistoriPresensi =
            List<Map<String, dynamic>>.from(json.decode(response.body));
        DateTime today = DateTime.now();
        if (mounted) {
          setState(() {
            historiPresensi = allHistoriPresensi.where((histori) {
              String presenceDate = histori['date'];
              DateTime date = DateFormat('yyyy-MM-dd').parse(presenceDate);
              return date.isBefore(today) || date.isAtSameMomentAs(today);
            }).toList();
          });
        }
        setLoading(false);
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(false);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet anda tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan: ${e.toString()}');
      }
      setLoading(false);
    }
  }

  void _showErrorToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  void setCurrentYear() {
    final now = DateTime.now();
    currentYear = now.year.toString();
  }

  void setCurrentMonth() {
    final now = DateTime.now();
    int indexOfMonth = now.month;
    selectedMonth = months.elementAt(indexOfMonth - 1);
  }

  String formatDate(String date) {
    DateTime parsedDate = DateFormat('yyyy-MM-dd').parse(date);
    return DateFormat('EEEE, d MMMM y', 'id_ID').format(parsedDate);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final isSmallScreen = screenWidth <= 600;

    // Define responsive padding and font sizes based on screen size
    final double horizontalPadding = isSmallScreen ? 8.0 : 16.0;
    final double verticalPadding = isSmallScreen ? 8.0 : 16.0;
    final double titleFontSize = isSmallScreen ? 14.0 : 16.0;
    final double subTitleFontSize = isSmallScreen ? 14.0 : 15.0;
    final double cardFontSize = isSmallScreen ? 14.0 : 16.0;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: isLoading
          ? null
          : AppBar(
              backgroundColor: Colors.blue,
              automaticallyImplyLeading: false,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 8.0 * MediaQuery.of(context).textScaleFactor),
                  const Text(
                    'Riwayat Presensi',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              elevation: 0, // Set elevation ke 0 untuk menghapus shadow
            ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(
                            10.0 * MediaQuery.of(context).textScaleFactor),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: horizontalPadding),
                      child: Row(
                        children: [
                          Text(
                            'Presensi Bulan: ', // Added title
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: titleFontSize,
                            ),
                          ),
                          SizedBox(width: isSmallScreen ? 4.0 : 8.0),
                          DropdownButton<String>(
                            value: selectedMonth,
                            onChanged: (String? newValue) {
                              setState(() {
                                selectedMonth = newValue!;
                              });
                              // Konversi nama bulan menjadi nilai numerik
                              int monthIndex =
                                  months.indexOf(selectedMonth) + 1;
                              fetchHistoriPresensi(
                                monthIndex.toString(),
                                currentYear,
                              );
                            },
                            items: months.map<DropdownMenuItem<String>>(
                              (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                      fontSize: subTitleFontSize,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                );
                              },
                            ).toList(),
                          ),
                          SizedBox(width: isSmallScreen ? 4.0 : 8.0),
                          Text(
                            currentYear,
                            style: TextStyle(
                              fontSize: subTitleFontSize,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: historiPresensi.length,
                      itemBuilder: (context, index) {
                        var histori = historiPresensi[index];
                        String presentName = histori['present_name'];
                        String timeIn = histori['time_in'] ?? '';
                        String timeOut = histori['time_out'];
                        String presenceDate = histori['date'];
                        String status = histori['status'] ?? '';

                        return Padding(
                          padding: EdgeInsets.all(verticalPadding),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            color: presentName == 'Izin'
                                ? Colors.lightBlue
                                : presentName == 'Sakit'
                                    ? Colors.indigo
                                    : status == 'Terlambat'
                                        ? Colors.yellow[800]
                                        : presentName == 'Hadir'
                                            ? Colors.green
                                            : Colors.red,
                            child: Padding(
                              padding: EdgeInsets.all(cardFontSize),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: verticalPadding / 2),
                                      Text(
                                        formatDate(presenceDate),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: subTitleFontSize,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(height: verticalPadding / 3),
                                      Text(
                                        'Masuk: $timeIn',
                                        style: TextStyle(
                                          fontSize: cardFontSize,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(height: verticalPadding / 3),
                                      Text(
                                        'Pulang: $timeOut',
                                        style: TextStyle(
                                          fontSize: cardFontSize,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    presentName == 'Izin' ||
                                            presentName == 'Sakit'
                                        ? presentName
                                        : status == 'Terlambat'
                                            ? '$presentName (Terlambat)'
                                            : presentName,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: cardFontSize,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
      ),
      bottomNavigationBar: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: CustomBottomNavigationBar(
          selectedIndex: selectedIndex,
          onIndexChanged: (index) {
            setState(() {
              selectedIndex = index;
            });
            switch (index) {
              case 0:
                // Navigate to  home
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
                break;
              case 1:
                // Navigate to Izin
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const IzinScreen()),
                );
                break;
              case 2:
                // Navigate to Histori
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HistoriScreen()),
                );
                break;
              case 3:
                // Stay on Profil
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilScreen()),
                );
                break;
            }
          },
        ),
      ),
    );
  }
}
