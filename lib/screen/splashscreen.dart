import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:presensi/screen/home_screen.dart';
import 'package:presensi/screen/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splashscreen extends StatefulWidget {
  const Splashscreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  bool _isLoggedIn = false;

  @override
  void initState() {
    super.initState();
    _loadWidget();
    _askPermission();
  }

  Future<void> _askPermission() async {
    // ignore: unused_local_variable
    Map<Permission, PermissionStatus> statuses = await [
      Permission.locationWhenInUse,
      Permission.camera,
      Permission.storage,
    ].request();
  }

  Future<void> _checkIfLoggedIn() async {
    final preferences = await SharedPreferences.getInstance();
    setState(() {
      _isLoggedIn = preferences.getBool('isLoggedIn') ?? false;
    });
  }

  void _navigationPage() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) =>
              _isLoggedIn ? const HomeScreen() : const LoginScreen()),
    );
  }

  Future<Timer> _loadWidget() async {
    await _checkIfLoggedIn();
    const Duration duration = Duration(seconds: 10);
    return Timer(duration, _navigationPage);
  }

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;

    final double logoWidth = screenWidth * 0.7;
    final double fontSize = screenWidth <= 360 ? 16.0 : 18.0;
    final double padding = screenWidth <= 360 ? 10.0 : 20.0;
    final double circleSize = 35.0 * MediaQuery.of(context).textScaleFactor;
    final double versionSpacing = 2.0 * MediaQuery.of(context).textScaleFactor;
    final double kaellAppsWidth = Get.width * 0.1;

    return Container(
      height: screenHeight,
      width: screenWidth,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Colors.blue,
            Colors.blue,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Align(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/home-logo.png',
                    width: logoWidth,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SpinKitFadingCircle(
                    color: Colors.white,
                    size: circleSize,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0.1 * screenHeight),
                  ),
                  SizedBox(height: padding),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      const Spacer(),
                      Text(
                        'Teacher Attendance System',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: fontSize,
                        ),
                      ),
                      SizedBox(width: versionSpacing),
                      Text(
                        ' version 1.7.0',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                        ),
                      ),
                      const Spacer(),
                    ],
                  ),
                  SizedBox(height: padding),
                  Container(
                    width: Get.width * 0.6,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Spacer(),
                          Image.asset(
                            'assets/images/logo.png',
                            width: kaellAppsWidth,
                            fit: BoxFit.fitHeight,
                          ),
                          const Text("  "),
                          Image.asset(
                            'assets/images/logo-sd.png',
                            width: kaellAppsWidth,
                            fit: BoxFit.fitHeight,
                          ),
                          const Spacer(),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                      height: 30.0 * MediaQuery.of(context).textScaleFactor),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
