import 'dart:async';
import 'dart:convert';

import 'package:app_settings/app_settings.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:presensi/action/presensi_action.dart';
import 'package:presensi/screen/histori_screen.dart';
import 'package:presensi/screen/izin_screen.dart';
import 'package:presensi/screen/profil_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../util/navbar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;

  Map<String, dynamic> userData = {};
  Map<String, dynamic> attendanceData = {};
  Map<String, dynamic> presence = {};

  bool isLoading = true;
  bool isPresensi = true;

  String selectedMonth = '';
  String currentYear = '';
  String greeting = '';
  String firstName = '';
  String currentUser = '';
  String dateNow = '';
  String presenceTimeIN = '';
  String presenceTimeOUT = '';

  Stream<String>? currentTimeInSeconds;

  List<String> months = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ];

  @override
  void initState() {
    super.initState();
    _checkInternetConnectivity();
  }

  Future<void> _checkInternetConnectivity() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    _handleConnectivityResult(connectivityResult);
    Connectivity().onConnectivityChanged.listen((_handleConnectivityResult));
  }

  void _handleConnectivityResult(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      _showErrorToast('Tidak ada koneksi internet');
      setLoading(true);
    } else {
      _getEmailFromSharedPrefs();
      _checkPermission();
      setTimePresence();
      setDateNow();
      setCurrentYear();
      setCurrentMonth();
      setGreeting();
      setLoading(false);
    }
  }

  Future<void> fetchUserProfile(String email) async {
    setLoading(true);
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-profil.php?email=$email');
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            userData = jsonResponse;
          });
          startTimer();
          setLoading(false);
        }
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(true);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
      setLoading(false);
    }
  }

  Future<void> _getEmailFromSharedPrefs() async {
    try {
      final SharedPreferences preferences;
      preferences = await SharedPreferences.getInstance();
      var email = preferences.getString('email') ?? '';
      var currentUser = preferences.getString('id') ?? '';
      var currentName = preferences.getString('employees_name');
      fetchUserProfile(email);
      fetchAttendanceData(currentUser, selectedMonth, currentYear);
      setFirstName(currentName!);
      fecthTime(currentUser);
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
      setLoading(false);
    }
  }

  Future<void> fecthTime(String currentUser) async {
    setLoading(true);
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-time.php?employees_id=$currentUser');
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            presence = jsonResponse;
            setTimePresence();
          });
          setLoading(false);
        }
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(true);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
      setLoading(false);
    }
  }

  Future<void> fetchAttendanceData(
      String currentUser, String selectedMonth, String selectedYear) async {
    setLoading(true);
    int monthIndex = selectedMonth.length > 2
        ? months.indexOf(selectedMonth) + 1
        : int.parse(selectedMonth);
    try {
      var url = Uri.parse(
          'https://presensi.booksinternationals.online/api/api-home.php?employeesId=$currentUser&selectedMonth=$monthIndex&selectedYears=$selectedYear');
      var response = await http.get(url);

      if (response.statusCode == 200) {
        var jsonResponse = jsonDecode(response.body);
        if (mounted) {
          setState(() {
            attendanceData = jsonResponse;
          });
          setLoading(false);
        }
      } else {
        _showErrorToast('Koneksi internet anda tidak stabil');
        setLoading(true);
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
      setLoading(false);
    }
  }

  void _checkPermission() async {
    try {
      final status = await Permission.camera.request();
      final hasPermission = await Permission.location.request();

      if (status.isGranted) {
        if (mounted) {
          setState(() {
            isPresensi = false;
          });
        }
      } else {
        _showErrorToast(
            'Izin Kamera Diperlukan, Harap aktifkan izin kamera untuk menggunakan aplikasi ini.');
        Future.delayed(const Duration(seconds: 2), () {
          _openAppSettings();
        });
        if (mounted) {
          setState(() {
            isPresensi = true;
          });
        }
      }

      if (hasPermission.isGranted) {
        if (mounted) {
          setState(() {
            isPresensi = false;
          });
        }
      } else {
        _showErrorToast(
            'Izin Lokasi Diperlukan, Harap aktifkan izin lokasi untuk menggunakan aplikasi ini.');
        Future.delayed(const Duration(seconds: 2), () {
          _openAppSettings();
        });
        if (mounted) {
          setState(() {
            isPresensi = true;
          });
        }
      }
    } catch (e) {
      if (e is Exception) {
        _showErrorToast('Koneksi internet tidak stabil');
      } else {
        _showErrorToast('Terjadi kesalahan:  ${e.toString()}');
      }
      setLoading(false);
    }
  }

  void _openAppSettings() {
    AppSettings.openAppSettings();
  }

  void setLoading(bool isLoading) {
    if (mounted) {
      setState(() {
        this.isLoading = isLoading;
      });
    }
  }

  void _showErrorToast(String message) {
    if (mounted) {
      Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void setCurrentYear() {
    final now = DateTime.now();
    currentYear = now.year.toString();
  }

  void setCurrentMonth() {
    final now = DateTime.now();
    int indexOfMonth = now.month;
    selectedMonth = months.elementAt(indexOfMonth - 1);
  }

  void setDateNow() {
    dateNow = DateFormat('d MMM y').format(DateTime.now());
  }

  void setGreeting() {
    final now = DateTime.now();
    if (now.hour >= 0 && now.hour < 12) {
      greeting = 'Selamat Pagi';
    } else if (now.hour >= 12 && now.hour < 15) {
      greeting = 'Selamat Siang';
    } else if (now.hour >= 15 && now.hour < 18) {
      greeting = 'Selamat Sore';
    } else {
      greeting = 'Selamat Malam';
    }
  }

  void setFirstName(String currentName) {
    firstName = currentName.split(' ')[0];
  }

  void startTimer() {
    currentTimeInSeconds = Stream<String>.periodic(
      const Duration(seconds: 1),
      (count) {
        final now = DateTime.now();
        return '${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}:${now.second.toString().padLeft(2, '0')}';
      },
    ).takeWhile((_) => mounted).asBroadcastStream();
  }

  void setTimePresence() {
    if (mounted) {
      if (presence['time_in'] == null ||
          presence['time_in'].toString() == '00:00:00') {
        presenceTimeIN = "Belum Presensi";
      } else {
        presenceTimeIN = presence['time_in'].toString();
      }
      if (presence['time_out'] == null ||
          presence['time_out'].toString() == '00:00:00') {
        presenceTimeOUT = "Belum Presensi";
      } else {
        presenceTimeOUT = presence['time_out'].toString();
      }
    }
  }

  Widget buildAttendanceCard(
    String title,
    IconData icon,
    String subtext,
  ) {
    final double cardHeight = 80.0 * MediaQuery.of(context).textScaleFactor;
    final double cardIconSize = 24.0 * MediaQuery.of(context).textScaleFactor;
    final double cardTitleFontSize =
        8.0 * MediaQuery.of(context).textScaleFactor;
    final double cardSubtextFontSize =
        7.0 * MediaQuery.of(context).textScaleFactor;
    Color cardColor = Colors.white;

    if (isPresensi ||
        subtext != "Belum Presensi" ||
        (title == "Masuk" && subtext != "Belum Presensi") ||
        (title == "Pulang" && subtext != "Belum Presensi")) {
      return Expanded(
        child: buildCard(
          title,
          height: cardHeight,
          icon: icon,
          subtext: subtext,
          cardColor: cardColor,
          cardIconSize: cardIconSize,
          cardTitleFontSize: cardTitleFontSize,
          cardSubtextFontSize: cardSubtextFontSize,
        ),
      );
    } else if (title == "Masuk") {
      return Expanded(
        child: GestureDetector(
          onTap: () async {
            final SharedPreferences preferences =
                await SharedPreferences.getInstance();
            await preferences.setString('presensi', 'Masuk');
            // ignore: use_build_context_synchronously
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const PresensiScreen()),
            );
          },
          child: buildCard(
            title,
            height: cardHeight,
            icon: icon,
            subtext: subtext,
            cardColor: cardColor,
            cardIconSize: cardIconSize,
            cardTitleFontSize: cardTitleFontSize,
            cardSubtextFontSize: cardSubtextFontSize,
          ),
        ),
      );
    } else if (presenceTimeIN != "Belum Presensi") {
      if (title == "Pulang") {
        return Expanded(
          child: GestureDetector(
            onTap: () async {
              final SharedPreferences prefs =
                  await SharedPreferences.getInstance();
              await prefs.setString('presensi', 'Keluar');
              // ignore: use_build_context_synchronously
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const PresensiScreen()),
              );
            },
            child: buildCard(
              title,
              height: cardHeight,
              icon: icon,
              subtext: subtext,
              cardColor: cardColor,
              cardIconSize: cardIconSize,
              cardTitleFontSize: cardTitleFontSize,
              cardSubtextFontSize: cardSubtextFontSize,
            ),
          ),
        );
      } else {
        return Expanded(
          child: buildCard(
            title,
            height: cardHeight,
            icon: icon,
            subtext: subtext,
            cardColor: cardColor,
            cardIconSize: cardIconSize,
            cardTitleFontSize: cardTitleFontSize,
            cardSubtextFontSize: cardSubtextFontSize,
          ),
        );
      }
    } else {
      return Expanded(
        child: buildCard(
          title,
          height: cardHeight,
          icon: icon,
          subtext: subtext,
          cardColor: cardColor,
          cardIconSize: cardIconSize,
          cardTitleFontSize: cardTitleFontSize,
          cardSubtextFontSize: cardSubtextFontSize,
        ),
      );
    }
  }

  Widget buildCard(
    String title, {
    required double height,
    required IconData icon,
    required String subtext,
    required Color cardColor,
    required double cardIconSize,
    required double cardTitleFontSize,
    required double cardSubtextFontSize,
  }) {
    final double cardMargin = 16.0 * MediaQuery.of(context).textScaleFactor;
    final double cardPadding = 16.0 * MediaQuery.of(context).textScaleFactor;
    final double cardIconSize = 24.0 * MediaQuery.of(context).textScaleFactor;
    final double cardTitleFontSize =
        11.0 * MediaQuery.of(context).textScaleFactor;
    final double cardSubtextFontSize =
        9.0 * MediaQuery.of(context).textScaleFactor;

    return Container(
      height: height,
      margin: EdgeInsets.all(cardMargin),
      padding: EdgeInsets.all(cardPadding),
      decoration: BoxDecoration(
        color: title == 'Masuk'
            ? Colors.blue[900]
            : title == 'Pulang'
                ? Colors.blue
                : Colors.orange[500],
        borderRadius: BorderRadius.circular(
            10.0 * MediaQuery.of(context).textScaleFactor),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(
                icon,
                color: Colors.white,
                size: cardIconSize,
              ),
              const SizedBox(width: 8.0),
              Text(
                title,
                style: TextStyle(
                  fontSize: cardTitleFontSize,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          const SizedBox(height: 12.0),
          Text(
            subtext,
            style: TextStyle(
              fontSize: cardSubtextFontSize,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;

    // Define responsive dimensions based on screen size
    final double greetingFontSize =
        14.0 * MediaQuery.of(context).textScaleFactor;
    final double cardMargin = 12.0 * MediaQuery.of(context).textScaleFactor;
    final double cardPadding = 12.0 * MediaQuery.of(context).textScaleFactor;
    final double dropdownFontSize =
        16.0 * MediaQuery.of(context).textScaleFactor;

    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 227, 227, 227),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0 * MediaQuery.of(context).textScaleFactor,
        ),
        child: isLoading
            ? const Center(
                child: SpinKitCircle(
                  color: Colors.blue,
                  size: 50.0,
                ),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 0.05 * screenHeight),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      Image.asset(
                        'assets/images/home-logo.png',
                        width: Get.width * 0.60,
                        fit: BoxFit.fitHeight,
                      ),
                      const Spacer(),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(cardMargin, 0.05 * screenHeight,
                        cardMargin, cardMargin),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(
                            10.0 * MediaQuery.of(context).textScaleFactor),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ]),
                    child: Padding(
                      padding: EdgeInsets.all(cardPadding),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                greeting,
                                style: TextStyle(
                                  fontSize: greetingFontSize,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              Text(
                                dateNow,
                                style: TextStyle(
                                  fontSize: greetingFontSize,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 10.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Halo $firstName !',
                                style: const TextStyle(
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              const SizedBox(height: 8.0),
                              StreamBuilder<String>(
                                stream: currentTimeInSeconds,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    String currentTime = snapshot.data!;
                                    return Text(
                                      currentTime,
                                      style: TextStyle(
                                        fontSize: 14.0 *
                                            MediaQuery.of(context)
                                                .textScaleFactor,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    );
                                  } else {
                                    return Text(
                                      "00:00:00",
                                      style: TextStyle(
                                        fontSize: 14.0 *
                                            MediaQuery.of(context)
                                                .textScaleFactor,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    );
                                  }
                                },
                              ),
                            ],
                          ),
                          const SizedBox(height: 8.0),
                          Row(
                            children: [
                              buildAttendanceCard(
                                'Masuk',
                                Icons.qr_code,
                                presenceTimeIN,
                              ),
                              buildAttendanceCard(
                                'Pulang',
                                Icons.qr_code,
                                presenceTimeOUT,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: cardMargin,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Presensi Bulan: ",
                          style: TextStyle(
                            fontSize: dropdownFontSize,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(width: 8.0),
                        DropdownButton<String>(
                          value: selectedMonth,
                          onChanged: (String? newValue) async {
                            var connectivityResult =
                                await Connectivity().checkConnectivity();
                            if (connectivityResult == ConnectivityResult.none) {
                              _showErrorToast('Tidak ada koneksi internet');
                            } else {
                              if (mounted) {
                                setState(() {
                                  selectedMonth = newValue!;
                                });
                              }
                              int monthIndex =
                                  months.indexOf(selectedMonth) + 1;
                              currentUser = userData['id'];
                              fetchAttendanceData(
                                currentUser,
                                monthIndex.toString(),
                                currentYear,
                              );
                            }
                          },
                          items: months.map<DropdownMenuItem<String>>(
                            (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: TextStyle(
                                    fontSize: dropdownFontSize,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                        const SizedBox(width: 8.0),
                        Text(
                          currentYear,
                          style: TextStyle(
                            fontSize: dropdownFontSize,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            buildAttendanceCard(
                              "Hadir",
                              Icons.login,
                              '${attendanceData['total_kehadiran'] ?? 0} hari',
                            ),
                            buildAttendanceCard(
                              'Terlambat',
                              Icons.timer,
                              '${attendanceData['total_terlambat'] ?? 0} hari',
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            buildAttendanceCard(
                              'Sakit',
                              Icons.local_hospital,
                              '${attendanceData['total_sakit'] ?? 0} hari',
                            ),
                            buildAttendanceCard(
                              'Izin',
                              Icons.assignment,
                              '${attendanceData['total_izin'] ?? 0} hari',
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
      ),
      bottomNavigationBar: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: CustomBottomNavigationBar(
          selectedIndex: selectedIndex,
          onIndexChanged: (index) {
            selectedIndex = index;
            switch (index) {
              case 0:
                // Navigate to  home
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
                break;
              case 1:
                // Navigate to Izin
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const IzinScreen()),
                );
                break;
              case 2:
                // Navigate to Histori
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HistoriScreen()),
                );
                break;
              case 3:
                // Stay on Profil
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilScreen()),
                );
                break;
            }
          },
        ),
      ),
    );
  }
}
